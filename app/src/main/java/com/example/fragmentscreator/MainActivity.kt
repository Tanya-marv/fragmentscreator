package com.example.fragmentscreator

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Context
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewpager2.widget.ViewPager2
import com.example.fragmentscreator.navigator.FragmentNavigator
import com.example.fragmentscreator.presentation.HomeFragment
import com.example.fragmentscreator.presentation.adapter.MainAdapter

class MainActivity : AppCompatActivity(), FragmentNavigator {

    private lateinit var viewPager: ViewPager2

    private val adapter = MainAdapter(this)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        viewPager = findViewById(R.id.view_pager)
        viewPager.adapter = adapter

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = "Channel"
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel(HomeFragment.CHANNEL_ID, name, importance).apply {
                lockscreenVisibility = Notification.VISIBILITY_PUBLIC
                enableVibration(true)
            }
            val notificationManager: NotificationManager =
                this.getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    override fun onBackPressed() {
        if (viewPager.currentItem == 0) {
            super.onBackPressed()
        } else {
            viewPager.currentItem = viewPager.currentItem - 1
        }
    }

    override fun addFragment() {
        adapter.addFragment()
        viewPager.currentItem = adapter.currentIndex()
    }

    override fun removeFragment() {
        viewPager.currentItem = adapter.currentIndex() - 1
        adapter.popFragment()
    }
}