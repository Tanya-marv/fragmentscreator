package com.example.fragmentscreator.navigator

interface FragmentNavigator {

    fun addFragment()

    fun removeFragment()
}