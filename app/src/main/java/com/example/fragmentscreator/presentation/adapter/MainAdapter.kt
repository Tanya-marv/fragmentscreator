package com.example.fragmentscreator.presentation.adapter

import androidx.fragment.app.FragmentActivity
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.example.fragmentscreator.presentation.HomeFragment

class MainAdapter(
    activity: FragmentActivity,
    private val dataList: MutableList<Int> = mutableListOf(0)
) : FragmentStateAdapter(activity) {

    override fun getItemCount(): Int = dataList.count()

    override fun createFragment(position: Int) = HomeFragment.newInstance(dataList[position])

    fun popFragment() {
        dataList.removeLast()
        notifyItemRemoved(dataList.lastIndex)
    }

    fun addFragment() {
        dataList.add(dataList.last() + 1)
        notifyItemRangeChanged(dataList.lastIndex - 1, dataList.lastIndex)
    }

    fun currentIndex() = dataList.lastIndex
}