package com.example.fragmentscreator.presentation

import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.core.os.bundleOf
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import com.example.fragmentscreator.MainActivity
import com.example.fragmentscreator.R
import com.example.fragmentscreator.navigator.FragmentNavigator
import com.google.android.material.floatingactionbutton.FloatingActionButton

class HomeFragment : Fragment(R.layout.fragment_create_notification) {

    private lateinit var btnRemove: FloatingActionButton
    private lateinit var btnAdd: FloatingActionButton
    private lateinit var tvTitle: TextView

    private var navigator: FragmentNavigator? = null

    override fun onAttach(context: Context) {
        super.onAttach(context)
        navigator = parentFragment as? FragmentNavigator
            ?: context as? FragmentNavigator
                    ?: throw IllegalArgumentException("Not implemented")
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val id = arguments?.getInt(ARG_FRAGMENT_ID)
        view.findViewById<TextView>(R.id.tv_page).text = arguments?.getInt(ARG_FRAGMENT_ID).toString()
        view.findViewById<FloatingActionButton>(R.id.btn_remove_fragment).isVisible = id != 0
        arguments?.let { getPageNumberNotification(it.getInt(ARG_FRAGMENT_ID)) }
        setupViews()
    }

    private fun getPageNumberNotification(fragmentId: Int) {
        view?.findViewById<ImageView>(R.id.iv_bg_notification)?.setOnClickListener {

            Toast.makeText(context, "Selected position: ${fragmentId + 1}", Toast.LENGTH_SHORT).show()

            val showIntent = Intent(context, MainActivity::class.java).putExtra("id", fragmentId)
            val showPendingIntent = PendingIntent.getActivity(context, 0, showIntent, 0)

            val builder = NotificationCompat.Builder(requireContext(), CHANNEL_ID)
                .setSmallIcon(R.drawable.ic_launcher_foreground)
                .setContentTitle("Chat heads active")
                .setContentText("Notification: ${fragmentId + 1}")
                .setPriority(NotificationCompat.PRIORITY_HIGH)
                .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                .setFullScreenIntent(showPendingIntent, true)
                .setCategory(NotificationCompat.CATEGORY_ALARM)
                .setGroup(GROUP_ID)
                .setGroupSummary(true)
                .setAutoCancel(true)

            with(NotificationManagerCompat.from(requireContext())) {
                notify(NOTIFICATION_ID, builder.build())
            }
        }
    }

    private fun setupViews() {
        btnRemove = requireView().findViewById(R.id.btn_remove_fragment)
        btnAdd = requireView().findViewById(R.id.btn_add_fragment)
        tvTitle = requireView().findViewById(R.id.tv_page)

        tvTitle.text = arguments?.getInt(ARG_FRAGMENT_ID, -1)?.plus(1).toString()
        btnAdd.setOnClickListener {
            navigator?.addFragment()
        }
        btnRemove.setOnClickListener {
            navigator?.removeFragment()
        }
    }

    companion object {

        private const val NOTIFICATION_ID = 101

        private const val GROUP_ID = "GROUP_ID"

        const val CHANNEL_ID = "CHANNEL_ID"

        private const val ARG_FRAGMENT_ID = "ARG_FRAGMENT_ID"

        fun newInstance(fragmentId: Int) = HomeFragment().apply {
            arguments = bundleOf(
                ARG_FRAGMENT_ID to fragmentId
            )
        }
    }
}